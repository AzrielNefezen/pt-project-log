﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Text;
using ProtoBuf;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using PCSC;
using System.Text.RegularExpressions;
using System.Threading;


namespace TSproject
{
    public partial class Form1 : Form
    {
        static bool hadError = false;
        static bool displayed = false;
        static string imie;
        static string nazwisko;
        static string pesel;
        static string nr_indeksu;
        public Thread sender;

        static void CheckErr(SCardError err)
        {
            if (err != SCardError.Success)
            {
                hadError = true;
                throw new PCSCException(err,
                    SCardHelper.StringifyError(err));
            }
        }

        public void sendRequest()
        {
            

            MethodInvoker Sender = async delegate()
            {

                try
                {

                    // Establish SCard context
                    SCardContext hContext = new SCardContext();
                    hContext.Establish(SCardScope.System);

                    // Retrieve the list of Smartcard readers
                    string[] szReaders = hContext.GetReaders();
                    if (szReaders.Length <= 0)
                    {
                        if (!hadError)
                        {
                            hadError = true;
                            displayed = false;
                            throw new PCSCException(SCardError.NoReadersAvailable,
                                "Could not find any Smartcard reader.");

                        }
                    }


                    // Create a reader object using the existing context
                    SCardReader reader = new SCardReader(hContext);

                    // Connect to the card
                    SCardError err = reader.Connect(szReaders[0],
                        SCardShareMode.Shared,
                        SCardProtocol.T0 | SCardProtocol.T1);
                    CheckErr(err);
                    if (!displayed)
                    {
                        hadError = false;
                        displayed = true;



                        System.IntPtr pioSendPci;
                        switch (reader.ActiveProtocol)
                        {
                            case SCardProtocol.T0:
                                pioSendPci = SCardPCI.T0;
                                break;
                            case SCardProtocol.T1:
                                pioSendPci = SCardPCI.T1;
                                break;
                            default:
                                throw new PCSCException(SCardError.ProtocolMismatch,
                                    "Protocol not supported: "
                                    + reader.ActiveProtocol.ToString());
                        }

                        byte[] SELECT_MF = new byte[] { 0x00, 0xA4, 0x00, 0x0C, 0x02, 0x3F, 0x00 };
                        byte[] SELECT_DF_T0 = new byte[] { 0x00, 0xA4, 0x04, 0x00, 0x07, 0xD6, 0x16, 0x00, 0x00, 0x30, 0x01, 0x01 };
                        byte[] SELECT_DF_T1 = new byte[] { 0x00, 0xA4, 0x04, 0x04, 0x07, 0xD6, 0x16, 0x00, 0x00, 0x30, 0x01, 0x01 };
                        byte[] SELECT_ELS_T0 = new byte[] { 0x00, 0xA4, 0x00, 0x00, 0x02, 0x00, 0x02 };
                        byte[] SELECT_ELS_T1 = new byte[] { 0x00, 0xA4, 0x02, 0x04, 0x02, 0x00, 0x02 };
                        byte[] READ_ELS = new byte[] { 0x00, 0xB0, 0x00, 0x00, 0xFF };

                        //--
                        byte[] pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_MF, ref pbRecvBuffer);
                        CheckErr(err);


                        //--

                        //--
                        pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_DF_T0, ref pbRecvBuffer);
                        CheckErr(err);

                        //--

                        //--
                        pbRecvBuffer = new byte[256];

                        //--
                        pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_ELS_T0, ref pbRecvBuffer);
                        CheckErr(err);


                        //--

                        //--
                        pbRecvBuffer = new byte[2048];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, READ_ELS, ref pbRecvBuffer);
                        CheckErr(err);
                        nr_indeksu = "";
                        pesel = "";
                        nazwisko = "";
                        imie = "";

                        //--


                        hContext.Release();



                        System.Collections.Generic.List<char> splitters = new System.Collections.Generic.List<char> { };

                        char[] allowedChars = new char[] { 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż' };
                        for (int i = 0; i < 18; i++)
                        {

                            splitters.Add(allowedChars[i]);
                        }
                        char[] chars = System.Text.Encoding.UTF8.GetChars(pbRecvBuffer, 0, pbRecvBuffer.Length);



                        for (int i = 0; i < chars.Length; i++)
                        {
                            if (i < 60) { chars[i] = ' '; }
                            else if (!(splitters.Contains(chars[i])) && (chars[i] < 32 || chars[i] > 122 || (chars[i] > 32 && chars[i] < 47)))
                            {
                                chars[i] = ' ';
                            }
                        }
                        string str = "";

                        foreach (char a in chars)
                        {
                            str += a;
                        }
                        string[] strarr2 = Regex.Split(str, "0?( ){3,}|(?<=([0-9]))( ){2,}[A-ZĄĆĘŁŃÓŚŹŻ]  |( ){2,}(?=[0-9])|(?<=[0-9][A-Z]*)( ){2,}(?=[A-Z])");
                        System.Collections.Generic.List<string> data = new System.Collections.Generic.List<string> { };
                        foreach (string str1 in strarr2)
                        {

                            if (str1 != String.Empty && str1 != " " && !(str1.Length < 3) && !str1.Contains("0s1"))
                            {
                                data.Add(str1.Replace("  ", " "));
                            }
                        }
                        string[] strarr = data.ToArray();

                        var uczelnia = strarr[1];
                        Form1.nazwisko = strarr[2];
                        Form1.imie = strarr[3];
                        Form1.nr_indeksu = strarr[4];
                        Form1.pesel = strarr[5];
                        var data_waznosci = strarr[6];
                        strarr[0] = strarr[strarr.Length - 1] = "";
                        string test = "";
                        foreach (string zxc in strarr)
                        {
                            {
                                for (int i = 0; i < 32; i++)
                                {
                                    test = zxc.Trim((Convert.ToChar(i)));
                                }
                            }
                        }
                        data_waznosci = data_waznosci.Substring(0, 4) + "-" + data_waznosci.Substring(4, 2) + "-" + data_waznosci.Substring(6, 2);

                        string IP = "169.254.30.223";
                        TcpClient Client1 = new TcpClient(); // tworzymy zewnętrznego klienta, imituje on aplikację kliencką
                        //Client1.Connect(IP, 1500); //łączymy się z serwerem
                        await Client1.ConnectAsync(IPAddress.Parse(IP), 10000); //łączymy się z serwerem



                        proto.req myReqO = new proto.req();

                        myReqO.imie = Form1.imie;
                        myReqO.nazwisko = Form1.nazwisko;
                        myReqO.nrindeksu = Form1.nr_indeksu;
                        myReqO.pesel = Form1.pesel;

                        // przygotowanie danych do wysłania -> wyjście: tablica typu byte
                        MemoryStream outputBuffer = new MemoryStream();
                        Serializer.SerializeWithLengthPrefix<proto.req>(outputBuffer, myReqO, PrefixStyle.Fixed32, 0);
                        byte[] outData = outputBuffer.ToArray();

                        //

                        NetworkStream ns = Client1.GetStream(); // utworzenie strumienia danych
                        ns.Write(outData, 0, outData.Length);  // wysłanie danych w formie bajtów


                        byte[] odbior = new byte[1024];

                        await ns.ReadAsync(odbior, 0, 1024);

                        //odbieranie danych do buforka i odczytywanie z niego
                        MemoryStream inputBuffer = new MemoryStream(odbior);
                        proto.res myReqI = Serializer.DeserializeWithLengthPrefix<proto.res>(inputBuffer, PrefixStyle.Fixed32);
                        inputBuffer.Dispose();



                        ns.Close();
                        outputBuffer.Dispose();

                        TextBox1.Text = imie;
                        TextBox2.Text = nazwisko;
                        TextBox3.Text = nr_indeksu;
                        textBox7.Text = pesel;
                        textBox4.Text = myReqI.odpowiedz;
                        textBox5.Text = myReqI.version;
                    }

                }

                catch (PCSCException ex)
                {
                    TextBox1.Text = "";
                    TextBox2.Text = "";
                    TextBox3.Text = "";
                    textBox7.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";

                    if (displayed)
                    {
                        displayed = false;
                        this.sender.Join(1000);
                        

                    }

                }


            };

            

            while (true)
            {
                this.sender.Join(1000);
                this.Invoke(Sender);
            }
        }


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
                      
            this.sender = new Thread(sendRequest);
            this.sender.IsBackground = true;
            this.sender.Start();

        }

        private void TextBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }


        private void TextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }


    }
}

