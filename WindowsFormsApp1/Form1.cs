﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ProtoBuf;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using MySql.Data.MySqlClient;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public List<Student> Students;
        public List<Student> Accepted;
        public List<Student> Rejected;
        public Dictionary<string, List<string>> Courses;
        TcpListener serwer;
        Thread receiver;
        TcpClient Klient;
        bool loggedIn = false;
        int IdProwadzacego;
        string server = "sql.freshwebsite.nazwa.pl";
        string database = "freshwebsite_4";
        string uid = "freshwebsite_4";
        string password = "Projekt123";
        string connectionString;
        List<ListViewItem> StudentsArray = new List<ListViewItem> { };


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            this.Students = new List<Student> { };
            this.Accepted = new List<Student> { };
            this.Rejected = new List<Student> { };
            this.Courses = new Dictionary<string, List<string>> { };

            label1.Text = "";

            listView1.View = View.Details;
            listView1.AllowColumnReorder = false;
            listView1.FullRowSelect = true;
            listView1.GridLines = true;
            listView1.Sorting = SortOrder.Ascending;


            listView1.Columns.Add("Nr Indeksu", listView1.Width / 4, HorizontalAlignment.Left);
            listView1.Columns.Add("Imię", listView1.Width / 4, HorizontalAlignment.Left);
            listView1.Columns.Add("Nazwisko", listView1.Width / 4, HorizontalAlignment.Left);
            listView1.Columns.Add("Data", listView1.Width / 4, HorizontalAlignment.Left);

            this.comboBox1.DataSource = Courses.Keys.ToList();
            this.comboBox2.DataSource = Courses.Values.ToList();
            if (!loggedIn)
            {
                this.button1.Enabled = false;
                this.button2.Enabled = false;
                this.button3.Enabled = false;
                this.button4.Enabled = false;
                this.button5.Enabled = false;
                this.comboBox1.Enabled = false;
                this.comboBox2.Enabled = false;
            }

        }

        public void startListener()
        {
            if (loggedIn)
            {
                serwer = new TcpListener(IPAddress.Any, 10000);
                serwer.Start();

                receiver = new Thread(DataReceiver);
                receiver.IsBackground = true;
                receiver.Start();
            }
        }

        public void stopListener()
        {
            if (!loggedIn)
            {
                receiver = null;
                if (receiver.ThreadState == ThreadState.Running)
                {
                    serwer.Stop();
                    receiver.Abort();
                }
            }
        }

        public void DataReceiver()
        {
            MethodInvoker ReceiveData = async delegate ()
        {
            Klient = await serwer.AcceptTcpClientAsync();

            NetworkStream ns = Klient.GetStream();

            byte[] odbior = new byte[1024];

            ns.Read(odbior, 0, 1024);

            MemoryStream inputBuffer = new MemoryStream(odbior);
            proto.req myReqI = Serializer.DeserializeWithLengthPrefix<proto.req>(inputBuffer, PrefixStyle.Fixed32);
            inputBuffer.Dispose();

            if (!(Students.Contains(new Student(myReqI.imie, myReqI.nazwisko, myReqI.nrindeksu, myReqI.pesel))))
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                Students.Add(new Student(myReqI.imie, myReqI.nazwisko, myReqI.nrindeksu, myReqI.pesel));
                string zapytanie = "INSERT INTO Student (IdStudenta, Imie, Nazwisko, StatusSt) VALUES (" + myReqI.nrindeksu + ", '" + myReqI.imie + "', '" + myReqI.nazwisko + "', " + "1" + ") ON DUPLICATE KEY UPDATE Imie ='" + myReqI.imie + "', Nazwisko ='" + myReqI.nazwisko + "', StatusSt = 1";
                MySqlCommand command = new MySqlCommand(zapytanie, connection);
                command.ExecuteNonQuery();
                foreach (Student singleStudent in Students.ToArray())
                {

                    ListViewItem item = new ListViewItem(new string[] { singleStudent.nrIndeksu.ToString(), singleStudent.imie, singleStudent.nazwisko, singleStudent.pesel });
                    StudentsArray.Add(item);
                    if (Accepted.Contains(singleStudent))
                    {
                        item.ForeColor = Color.Green;
                    }
                    if (Rejected.Contains(singleStudent))
                    {
                        item.ForeColor = Color.Red;
                    }

                }
                this.listView1.Items.Clear();
                this.listView1.Items.AddRange(StudentsArray.ToArray());
            }



            proto.res myReqO = new proto.res();

            string version = comboBox2.SelectedItem.ToString().Split(' ')[0] + " " + DateTime.Now.ToString("yyyy-MM-dd") + " " + comboBox2.SelectedItem.ToString().Split(' ')[1];
            string odpowiedz = comboBox1.SelectedItem.ToString();
            DateTime data = DateTime.Now;

            myReqO.version = version;
            myReqO.odpowiedz = odpowiedz;
            myReqO.data = data;


            MemoryStream outputBuffer = new MemoryStream();
            Serializer.SerializeWithLengthPrefix<proto.res>(outputBuffer, myReqO, PrefixStyle.Fixed32, 0);
            byte[] outData = outputBuffer.ToArray();

            ns.Write(outData, 0, outData.Length);

        };

            while (true)
            {
                this.Invoke(ReceiveData);
            }



        }

        private void button4_Click(object sender, EventArgs e)
        {

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            string godzina = comboBox2.SelectedItem.ToString().Split(' ')[1];
            string dzien = comboBox2.SelectedItem.ToString().Split(' ')[0];
            string zapytanie = "SELECT id FROM Zajecia WHERE idProwadzacego=" + IdProwadzacego + " AND godzina = '" + godzina + "' AND dzien ='" + dzien + "';";
            int IdPrzed = 0;
            MySqlCommand command = new MySqlCommand(zapytanie, connection);
            MySqlDataReader reader2 = command.ExecuteReader();
            StringBuilder csv = new StringBuilder();
            if (reader2.Read())
            {
                IdPrzed = reader2.GetInt32(0);
            }
            reader2.Close();
            zapytanie = "SELECT IdStudenta, godzina, data FROM Obecnosci WHERE IdZajec='" + IdPrzed + "';";
            command = new MySqlCommand(zapytanie, connection);
            reader2 = command.ExecuteReader();
            MySqlConnection connection2 = new MySqlConnection(connectionString);
            connection2.Open();
            while (reader2.Read())
            {
                string zapytanie2 = "SELECT DISTINCT IdStudenta, Imie, Nazwisko FROM Student WHERE IdStudenta='" + reader2.GetString(0) + "';";
                MySqlCommand command2 = new MySqlCommand(zapytanie2, connection2);
                MySqlDataReader reader3 = command2.ExecuteReader();
                while (reader3.Read())
                {
                    csv.AppendLine(reader3.GetString(0) + ";" + reader3.GetString(1) + ";" + reader3.GetString(2) + ";" + reader2.GetString(1) + ";" + reader2.GetString(2));
                }
                reader3.Close();
            }
            reader2.Close();
            SaveFileDialog save = new SaveFileDialog();
            save.AddExtension = true;
            save.DefaultExt = ".csv";
            save.Filter = "Plik .CSV | *.csv";
            save.FileName = "Obecnosci.csv";
            if (save.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(save.FileName, csv.ToString());
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand cmd = new MySqlCommand(
                "set net_write_timeout=99999; set net_read_timeout=99999", connection);
            cmd.ExecuteNonQuery();
            string zapytanie = "";

            foreach (ListViewItem a in listView1.SelectedItems)
            {
                if (!Rejected.Contains(new Student(a.SubItems[1].Text, a.SubItems[2].Text, a.SubItems[0].Text, a.SubItems[3].Text)))
                {
                    if (Accepted.Contains(new Student(a.SubItems[1].Text, a.SubItems[2].Text, a.SubItems[0].Text, a.SubItems[3].Text)))
                    {
                        Accepted.Remove(new Student(a.SubItems[1].Text, a.SubItems[2].Text, a.SubItems[0].Text, a.SubItems[3].Text));
                    }
                    Rejected.Add(new Student(a.SubItems[1].Text, a.SubItems[2].Text, a.SubItems[0].Text, a.SubItems[3].Text));
                    listView1.Items[listView1.Items.IndexOf(a)].ForeColor = Color.Red;
                    string godzina = comboBox2.SelectedItem.ToString().Split(' ')[1];
                    string dzien = comboBox2.SelectedItem.ToString().Split(' ')[0];
                    zapytanie = "SELECT id From Zajecia WHERE idProwadzacego = " + IdProwadzacego + " AND godzina ='" + godzina + "' AND dzien ='" + dzien + "'";
                    MySqlCommand command = new MySqlCommand(zapytanie, connection);
                    MySqlDataReader reader = command.ExecuteReader();
                    int IdZajec = 0;
                    if (reader.Read())
                    {
                        IdZajec = reader.GetInt32(0);
                    }
                    reader.Close();
                    zapytanie = "DELETE FROM Obecnosci WHERE IdStudenta =" + a.SubItems[0].Text + " AND IdZajec ='" + IdZajec + "' AND godzina ='" + godzina + "' AND data ='" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                    command = new MySqlCommand(zapytanie, connection);
                    command.ExecuteNonQuery();
                }

            }


        }

        private void button2_Click(object sender, EventArgs e)
        {

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand cmd = new MySqlCommand(
                "set net_write_timeout=99999; set net_read_timeout=99999", connection);
            cmd.ExecuteNonQuery();
            string zapytanie = "";
            foreach (ListViewItem a in listView1.SelectedItems)
            {
                if (!Accepted.Contains(new Student(a.SubItems[1].Text, a.SubItems[2].Text, a.SubItems[0].Text, a.SubItems[3].Text)))
                {
                    if (Rejected.Contains(new Student(a.SubItems[1].Text, a.SubItems[2].Text, a.SubItems[0].Text, a.SubItems[3].Text)))
                    {
                        Rejected.Remove(new Student(a.SubItems[1].Text, a.SubItems[2].Text, a.SubItems[0].Text, a.SubItems[3].Text));
                    }
                    Accepted.Add(new Student(a.SubItems[1].Text, a.SubItems[2].Text, a.SubItems[0].Text, a.SubItems[3].Text));
                    listView1.Items[listView1.Items.IndexOf(a)].ForeColor = Color.Green;
                    string godzina = comboBox2.SelectedItem.ToString().Split(' ')[1];
                    string dzien = comboBox2.SelectedItem.ToString().Split(' ')[0];
                    zapytanie = "SELECT id From Zajecia WHERE idProwadzacego = " + IdProwadzacego + " AND godzina ='" + godzina + "' AND dzien ='" + dzien + "'";
                    MySqlCommand command = new MySqlCommand(zapytanie, connection);
                    MySqlDataReader reader = command.ExecuteReader();
                    int IdZajec = 0;
                    if (reader.Read())
                    {
                        IdZajec = reader.GetInt32(0);
                    }
                    reader.Close();
                    zapytanie = "INSERT INTO Obecnosci (IdStudenta, IdZajec, godzina, data) VALUES (" + a.SubItems[0].Text + ", '" + IdZajec + "', '" + godzina + "', '" + DateTime.Now.ToString("yyyy-MM-dd") + "');";
                    command = new MySqlCommand(zapytanie, connection);
                    command.ExecuteNonQuery();
                }
            }
            connection.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!loggedIn)
            {
                Form2 loginForm = new Form2();
                var result = loginForm.ShowDialog();
                if (loginForm.DialogResult == DialogResult.OK)
                {
                    loggedIn = true;
                    startListener();
                    linkLabel1.Text = "Wyloguj";
                    label1.Text = loginForm.nazwaUzytkownika;
                    IdProwadzacego = loginForm.idPr;
                    Courses = loginForm.przedmioty;
                    this.comboBox1.DataSource = Courses.Keys.ToList();
                    this.button1.Enabled = true;
                    this.button2.Enabled = true;
                    this.button3.Enabled = true;
                    this.button4.Enabled = true;
                    this.button5.Enabled = true;
                    this.comboBox1.Enabled = true;
                    this.comboBox2.Enabled = true;


                }
            }
            else
            {
                loggedIn = false;
                IdProwadzacego = 0;
                stopListener();
                linkLabel1.Text = "Zaloguj";
                this.Students = new List<Student> { };
                this.Accepted = new List<Student> { };
                this.Rejected = new List<Student> { };
                this.Courses = new Dictionary<string, List<string>> { };


                listView1.View = View.Details;
                listView1.AllowColumnReorder = false;
                listView1.FullRowSelect = true;
                listView1.GridLines = true;
                listView1.Sorting = SortOrder.Ascending;
                listView1.Items.Clear();

                this.comboBox1.DataSource = new List<string> { };
                this.comboBox1.SelectedText = "";
                this.comboBox2.DataSource = new List<string> { };
                this.comboBox2.SelectedText = "";
                label1.Text = "";
                this.button1.Enabled = false;
                this.button2.Enabled = false;
                this.button3.Enabled = false;
                this.button4.Enabled = false;
                this.button5.Enabled = false;
                this.comboBox1.Enabled = false;
                this.comboBox2.Enabled = false;

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.comboBox2.DataSource = Courses[comboBox1.SelectedItem.ToString()];
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            receiver.Abort();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            string godzina = comboBox2.SelectedItem.ToString().Split(' ')[1];
            string dzien = comboBox2.SelectedItem.ToString().Split(' ')[0];
            string zapytanie = "SELECT id FROM Zajecia WHERE idProwadzacego=" + IdProwadzacego + " AND godzina = '" + godzina + "' AND dzien ='" + dzien + "';";
            int IdPrzed = 0;
            MySqlCommand command = new MySqlCommand(zapytanie, connection);
            MySqlDataReader reader2 = command.ExecuteReader();
            if (reader2.Read())
            {
                IdPrzed = reader2.GetInt32(0);
            }
            reader2.Close();
            zapytanie = "SELECT IdStudenta, godzina, data FROM Obecnosci WHERE IdZajec='" + IdPrzed + "'AND data ='" + DateTime.Now.ToString("yyyy-MM-dd") + "';";
            command = new MySqlCommand(zapytanie, connection);
            reader2 = command.ExecuteReader();
            MySqlConnection connection2 = new MySqlConnection(connectionString);
            connection2.Open();
            while (reader2.Read())
            {
                string zapytanie2 = "SELECT DISTINCT IdStudenta, Imie, Nazwisko FROM Student WHERE IdStudenta='" + reader2.GetString(0) + "';";
                MySqlCommand command2 = new MySqlCommand(zapytanie2, connection2);
                MySqlDataReader reader3 = command2.ExecuteReader();
                while (reader3.Read())
                {
                    if (!Students.Contains(new Student(reader3.GetString(1), reader3.GetString(2), reader3.GetString(0), DateTime.Now.ToString("yyyy-MM-dd"))))
                    {
                        Students.Add(new Student(reader3.GetString(1), reader3.GetString(2), reader3.GetString(0), DateTime.Now.ToString("yyyy-MM-dd")));
                        Accepted.Add(new Student(reader3.GetString(1), reader3.GetString(2), reader3.GetString(0), DateTime.Now.ToString("yyyy-MM-dd")));
                        ListViewItem NewItem = new ListViewItem(new string[] { reader3.GetString(1), reader3.GetString(2), reader3.GetString(0), DateTime.Now.ToString("yyyy-MM-dd") });
                        NewItem.ForeColor = Color.Green;
                        StudentsArray.Add(NewItem);
                    }
                }
                reader3.Close();
            }
            reader2.Close();
            this.listView1.Items.Clear();
            this.listView1.Items.AddRange(StudentsArray.ToArray());
            StudentsArray = new List<ListViewItem> { };


        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form3 list = new Form3();
            list.IdProwadzacego = this.IdProwadzacego;
            list.Courses = new Dictionary<string, List<string>> { };
            list.Courses.Add("", new List<string> { "" });
            list.Courses = list.Courses.Concat(Courses).ToDictionary(x => x.Key, x => x.Value);
            list.ShowDialog();

        }
    }
}
