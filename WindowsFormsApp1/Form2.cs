﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        MySqlConnection connection = new MySqlConnection();
        public string nazwaUzytkownika;
        public int idPr;
        public Dictionary<string, List<string>> przedmioty = new Dictionary<string, List<string>> { };
        public List<Student> StudentList = new List<Student>{};


        public Form2()
        {
            InitializeComponent();
            label1.Text = "";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Zalogujdobazy();
                string login2 = textBox1.Text;
                string haslo2 = textBox2.Text;
                MD5 md5Hash = MD5.Create();
                SHA1 sha = new SHA1CryptoServiceProvider();
                haslo2 = koduj(sha, md5Hash, haslo2);
                string zapytanie = "SELECT idProwadzacego, imie, nazwisko FROM Prowadzacy WHERE login='" + login2 + "' AND haslo='" + haslo2 + "' LIMIT 1;";
                MySqlCommand command = new MySqlCommand(zapytanie, connection);
                MySqlDataReader reader = command.ExecuteReader();



                if (reader.Read())
                {
                    idPr = reader.GetInt32(0);
                    label1.Text = reader.GetString(0);
                    label1.Text = reader.GetString(1) + " " + reader.GetString(2);
                    nazwaUzytkownika = label1.Text.ToString();
                    reader.Close();
                    zapytanie = "SELECT DISTINCT nazwa FROM Zajecia WHERE idProwadzacego='" + idPr + "';";
                    command = new MySqlCommand(zapytanie, connection);
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        przedmioty.Add(reader.GetString(0), new List<string> { });
                    }
                    reader.Close();
                    foreach (string przedmiot in przedmioty.Keys.ToArray())
                    {
                        zapytanie = "SELECT dzien, godzina FROM Zajecia WHERE nazwa='" + przedmiot + "';";
                        command = new MySqlCommand(zapytanie, connection);
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            przedmioty[przedmiot].Add(reader.GetString(0) + " " + reader.GetString(1));

                        }
                        reader.Close();
                    }

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }

                else
                {
                    label1.Text = "Niepoprawny Login i/lub hasło";
                }
            }
            catch (MySqlException ex)
            {
                label1.Text = "Wystąpił błąd.";
                MessageBox.Show(ex.ToString());
            }
        }
        static string koduj(SHA1 sha, MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            data = sha.ComputeHash(Encoding.UTF8.GetBytes(sBuilder.ToString()));
            sBuilder.Clear();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        protected void Zalogujdobazy()
        {
            string server = "sql.freshwebsite.nazwa.pl";
            string database = "freshwebsite_4";
            string uid = "freshwebsite_4";
            string password = "Projekt123";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand cmd = new MySqlCommand(
                "set net_write_timeout=99999; set net_read_timeout=99999", connection);
            cmd.ExecuteNonQuery();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}

