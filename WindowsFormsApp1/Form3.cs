﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp1
{
    public partial class Form3 : Form
    {
        public int IdProwadzacego;
        string server = "sql.freshwebsite.nazwa.pl";
        string database = "freshwebsite_4";
        string uid = "freshwebsite_4";
        string password = "Projekt123";
        string connectionString;
        List<string> Hours = new List<string> { };
        public Dictionary<string, List<string>> Courses;
        List<ListViewItem> StudentsArray;

        public Form3()
        {
            InitializeComponent();
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            Hours.Add("8:00");
            Hours.Add("9:45");
            Hours.Add("11:45");
            Hours.Add("13:30");
            Hours.Add("15:15");
            Hours.Add("17:00");
            Hours.Add("18:45");
            this.comboBox1.DataSource = Hours;

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }


        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            StudentsArray = new List<ListViewItem> { };
            string nrIndeksu = this.textBox4.Text != "" ? this.textBox4.Text : "";
            string nazwaPrzedmiotu = this.comboBox2.SelectedText.ToString();
            string dzien = dateTimePicker2.Checked ? this.dateTimePicker2.Text.ToString().Split(' ')[0].Replace(",", ""):"";
            string data = dateTimePicker2.Checked ? this.dateTimePicker2.Value.ToString().Split(' ')[0] : "";
            List<Student> listaObecnosci = new List<Student> { };
            if (data != "")
            {
                data = DateTime.Parse(data).ToString("yyyy-MM-dd");
            }
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand cmd = new MySqlCommand(
                "set net_write_timeout=99999; set net_read_timeout=99999", connection);
            cmd.ExecuteNonQuery();
            string zapytanie = "";
            string godzina = comboBox1.SelectedItem.ToString();
            zapytanie = "SELECT id From Zajecia WHERE idProwadzacego = " + IdProwadzacego + " AND godzina ='" + godzina +"'"+ (dzien!=""?" AND dzien ='" + dzien + "'":"");
            MySqlCommand command = new MySqlCommand(zapytanie, connection); 
            MySqlDataReader reader = command.ExecuteReader();
            int IdZajec = 0;
            if (reader.Read())
            {
                IdZajec = reader.GetInt32(0);
            }
            reader.Close();
            zapytanie = "SELECT IdStudenta, Imie, Nazwisko FROM Student WHERE IdStudenta IN (SELECT IdStudenta FROM Obecnosci WHERE " + (nrIndeksu != "" ? "IdStudenta ="
                + nrIndeksu + " AND " : "")
                + "godzina ='"
                + godzina+"'"
                + ((data != "") ? " AND data = '"
                + data + "'" : "")
                + " AND IdZajec = '"
                + IdZajec + "');";
            command = new MySqlCommand(zapytanie, connection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                StudentsArray.Add(new ListViewItem(new string[] { reader.GetInt32(0).ToString(), reader.GetString(1), reader.GetString(2), data }));
            }
            connection.Close();
            this.listView2.Items.Clear();
            this.listView2.Items.AddRange(StudentsArray.ToArray());

        }

        private void Form3_Load(object sender, EventArgs e)
        {
            this.comboBox2.DataSource = Courses.Keys.ToList();
            dateTimePicker2.ShowCheckBox = true;
            StudentsArray = new List<ListViewItem> { };
            listView2.View = View.Details;
            listView2.AllowColumnReorder = false;
            listView2.FullRowSelect = true;
            listView2.GridLines = true;
            listView2.Sorting = SortOrder.Ascending;


            listView2.Columns.Add("Nr Indeksu", listView2.Width / 4, HorizontalAlignment.Left);
            listView2.Columns.Add("Imię", listView2.Width / 4, HorizontalAlignment.Left);
            listView2.Columns.Add("Nazwisko", listView2.Width / 4, HorizontalAlignment.Left);
            listView2.Columns.Add("Data", listView2.Width / 4, HorizontalAlignment.Left);
        }
    }
}
