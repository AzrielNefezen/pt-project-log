﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Student: IEquatable<Student>
    {
        public String imie;
        public String nazwisko;
        public int nrIndeksu;
        public String pesel;

        public Student(String imie, String nazwisko, String nrIndeksu, String pesel)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.nrIndeksu = Convert.ToInt32(nrIndeksu);
            this.pesel = pesel;
        }

        public bool Equals(Student other)
        {
            if (this.nrIndeksu == other.nrIndeksu)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
