﻿using System;
using System.Text;

using PCSC;
using System.Text.RegularExpressions;

namespace HelloWorld
{
    class Program
    {
        static bool hadError;
        static bool displayed;
        static void CheckErr(SCardError err)
        {
            if (err != SCardError.Success)
            {
                hadError = true;
                throw new PCSCException(err,
                    SCardHelper.StringifyError(err));
            }
        }
        static void Main(string[] args)
        {
            displayed = false;
            hadError = false;

            while (true)
            {
                try
                {
                    string imie;
                    string nazwisko;
                    string pesel;
                    string nr_indeksu;
                    // Establish SCard context
                    SCardContext hContext = new SCardContext();
                    hContext.Establish(SCardScope.System);

                    // Retrieve the list of Smartcard readers
                    string[] szReaders = hContext.GetReaders();
                    if (szReaders.Length <= 0)
                    {
                        if (!hadError)
                        {
                            hadError = true;
                            displayed = false;
                            throw new PCSCException(SCardError.NoReadersAvailable,
                                "Could not find any Smartcard reader.");

                        }
                    }


                    // Create a reader object using the existing context
                    SCardReader reader = new SCardReader(hContext);

                    // Connect to the card
                    SCardError err = reader.Connect(szReaders[0],
                        SCardShareMode.Shared,
                        SCardProtocol.T0 | SCardProtocol.T1);
                    CheckErr(err);
                    if (!displayed)
                    {
                        hadError = false;
                        displayed = true;
                        Console.WriteLine("reader name: " + szReaders[0]);



                        System.IntPtr pioSendPci;
                        switch (reader.ActiveProtocol)
                        {
                            case SCardProtocol.T0:
                                pioSendPci = SCardPCI.T0;
                                break;
                            case SCardProtocol.T1:
                                pioSendPci = SCardPCI.T1;
                                break;
                            default:
                                throw new PCSCException(SCardError.ProtocolMismatch,
                                    "Protocol not supported: "
                                    + reader.ActiveProtocol.ToString());
                        }

                        byte[] SELECT_MF = new byte[] { 0x00, 0xA4, 0x00, 0x0C, 0x02, 0x3F, 0x00 };
                        byte[] SELECT_DF_T0 = new byte[] { 0x00, 0xA4, 0x04, 0x00, 0x07, 0xD6, 0x16, 0x00, 0x00, 0x30, 0x01, 0x01 };
                        byte[] SELECT_DF_T1 = new byte[] { 0x00, 0xA4, 0x04, 0x04, 0x07, 0xD6, 0x16, 0x00, 0x00, 0x30, 0x01, 0x01 };
                        byte[] SELECT_ELS_T0 = new byte[] { 0x00, 0xA4, 0x00, 0x00, 0x02, 0x00, 0x02 };
                        byte[] SELECT_ELS_T1 = new byte[] { 0x00, 0xA4, 0x02, 0x04, 0x02, 0x00, 0x02 };
                        byte[] READ_ELS = new byte[] { 0x00, 0xB0, 0x00, 0x00, 0xFF };

                        //--
                        byte[] pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_MF, ref pbRecvBuffer);
                        CheckErr(err);

                        Console.Write("select_mf: ");

                        //--

                        //--
                        pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_DF_T0, ref pbRecvBuffer);
                        CheckErr(err);

                        Console.Write("select_df_t0: ");
                        //--

                        //--
                        pbRecvBuffer = new byte[256];

                        //--
                        pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_ELS_T0, ref pbRecvBuffer);
                        CheckErr(err);

                        Console.Write("select_els_t0: ");
                        //--

                        //--
                        pbRecvBuffer = new byte[2048];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, READ_ELS, ref pbRecvBuffer);
                        CheckErr(err);
                        Console.Write("read_els: ");
                        nr_indeksu = "";
                        pesel = "";
                        nazwisko = "";
                        imie = "";

                        //--


                        hContext.Release();



                        System.Collections.Generic.List<char> splitters = new System.Collections.Generic.List<char> { };

                        char[] allowedChars = new char[] { 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż' };
                        for (int i = 0; i < 18; i++)
                        {

                            splitters.Add(allowedChars[i]);
                        }
                        char[] chars = System.Text.Encoding.UTF8.GetChars(pbRecvBuffer, 0, pbRecvBuffer.Length);



                        for (int i = 0; i < chars.Length; i++)
                        {
                            if (i < 60) { chars[i] = ' '; }
                            else if (!(splitters.Contains(chars[i])) && (chars[i] < 32 || chars[i] > 122))
                            {
                                chars[i] = ' ';
                            }
                        }
                        string str = "";

                        foreach (char a in chars)
                        {
                            Console.Write("{0:X2}", a +  "\t" + Convert.ToChar(a) + "\n");
                            str += a;
                        }
                        string[] strarr2 = Regex.Split(str, "0?( ){3,}|(?<=([0-9]))( ){2,}[A-ZĄĆĘŁŃÓŚŹŻ]  |( ){2,}(?=[0-9])|(?<=[0-9][A-Z]*)( ){2,}(?=[A-Z])");
                        System.Collections.Generic.List<string> data = new System.Collections.Generic.List<string> { };
                        foreach (string str1 in strarr2)
                        {

                            if (str1 != String.Empty && str1 != " " && !(str1.Length < 3))
                            {
                                data.Add(str1.Replace("  ", " "));
                            }
                        }
                        string[] strarr = data.ToArray();

                        var uczelnia = strarr[1];
                        nazwisko = strarr[2];
                        imie = strarr[3];
                        nr_indeksu = strarr[4];
                        pesel = strarr[5];
                        var data_waznosci = strarr[6];
                        strarr[0] = strarr[strarr.Length - 1] = "";
                        string test = "";
                        foreach (string zxc in strarr)
                        {
                            {
                                for (int i = 0; i < 32; i++)
                                {
                                    test = zxc.Trim((Convert.ToChar(i)));
                                }
                            }
                        }
                        data_waznosci = data_waznosci.Substring(0, 4) + "-" + data_waznosci.Substring(4, 2) + "-" + data_waznosci.Substring(6, 2);
                        Console.WriteLine("Uczelnia: " + uczelnia);
                        Console.WriteLine("Data ważności: " + Convert.ToDateTime(data_waznosci));
                        Console.WriteLine("Nr indeksu: " + nr_indeksu);
                        Console.WriteLine("Pesel: " + pesel);
                        Console.WriteLine("Imie: " + imie);
                        Console.WriteLine("Nazwisko: " + nazwisko);

                    }
                }

                catch (PCSCException ex)
                {
                    if (displayed)
                    {
                        displayed = false;
                        Console.WriteLine("Ouch: "
                            + ex.Message
                            + " (" + ex.SCardError.ToString() + ")");
                    }

                }
            }
        }
    }
}

