﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using System.Net;
using System.Net.Sockets;

namespace TSprojekt
{
    class Program
    {
        static void Main(string[] args)
        {
            
                TcpListener serwer = new TcpListener(IPAddress.Any, 1500);
            serwer.Start();
            while (true)
            {
                Console.WriteLine("Oczekiwanie na klienta");
             
            TcpClient Klient = serwer.AcceptTcpClient();
            NetworkStream ns = Klient.GetStream();

            Console.WriteLine("Polaczono z klientem");

            byte[] odbior = new byte[1024];

            ns.Read(odbior,0,1024);

            Console.WriteLine("Odebrano dane od klienta.");
            Console.WriteLine();

            // <--- przesłanie danych przez sieć ---> =

            // odebranie danych
            MemoryStream inputBuffer = new MemoryStream(odbior);
            proto.req myReqI = Serializer.DeserializeWithLengthPrefix<proto.req>(inputBuffer, PrefixStyle.Fixed32);
            inputBuffer.Dispose();

            Console.WriteLine("Odebrane dane:");
            Console.WriteLine("Imie: " + myReqI.imie);
            Console.WriteLine("Nazwisko: " + myReqI.nazwisko);
            Console.WriteLine("Nr indeksu: " + myReqI.nrindeksu);
            Console.WriteLine("Pesel: " + myReqI.pesel);

            Console.WriteLine("Wysylanie odpowiedzi do klienta");

            proto.res myReqO = new proto.res();

			//dane do wysłania
			string version = "0.1";
			string odpowiedz = "ok";
            DateTime data = DateTime.Now;
			//--------------------

			//dane dodawane do buforka
            myReqO.version = version;
            myReqO.odpowiedz = odpowiedz;
            myReqO.data=data;
			//--------------------

            // przygotowanie danych do wysłania -> wyjście: tablica typu byte
            MemoryStream outputBuffer = new MemoryStream();
            Serializer.SerializeWithLengthPrefix<proto.res>(outputBuffer, myReqO, PrefixStyle.Fixed32, 0);
            byte[] outData = outputBuffer.ToArray();

            ns.Write(outData, 0, outData.Length);
            }
            Console.ReadLine();
        }
    }
}