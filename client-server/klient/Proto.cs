﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace proto
{
    [ProtoContract]
    public class req
    {
        [ProtoMember(1)]
        public string imie;
        [ProtoMember(2)]
		public string nazwisko;
        [ProtoMember(3)]
        public string pesel;
        [ProtoMember(4)]
        public string nrindeksu;
    }
}

namespace proto
{
    [ProtoContract]
    public class res
    {
        [ProtoMember(1)]
		public string version;
        [ProtoMember(2)]
        public string odpowiedz;
		[ProtoMember(3)]
        public DateTime data;
    }
}
