﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using PCSC;
using System.Text.RegularExpressions;

namespace TSprojekt
{
    class Program
    {

        static bool hadError;
        static bool displayed;
        static string imie;
        static string nazwisko;
        static string pesel;
        static string nr_indeksu;
        static void CheckErr(SCardError err)
        {
            if (err != SCardError.Success)
            {
                hadError = true;
                throw new PCSCException(err,
                    SCardHelper.StringifyError(err));
            }
        }
        static void Main(string[] args)
        {
            displayed = false;
            hadError = false;

            while (true)
            {
                try
                {
                    
                    // Establish SCard context
                    SCardContext hContext = new SCardContext();
                    hContext.Establish(SCardScope.System);

                    // Retrieve the list of Smartcard readers
                    string[] szReaders = hContext.GetReaders();
                    if (szReaders.Length <= 0)
                    {
                        if (!hadError)
                        {
                            hadError = true;
                            displayed = false;
                            throw new PCSCException(SCardError.NoReadersAvailable,
                                "Could not find any Smartcard reader.");

                        }
                    }


                    // Create a reader object using the existing context
                    SCardReader reader = new SCardReader(hContext);

                    // Connect to the card
                    SCardError err = reader.Connect(szReaders[0],
                        SCardShareMode.Shared,
                        SCardProtocol.T0 | SCardProtocol.T1);
                    CheckErr(err);
                    if (!displayed)
                    {
                        hadError = false;
                        displayed = true;
                        Console.WriteLine("reader name: " + szReaders[0]);



                        System.IntPtr pioSendPci;
                        switch (reader.ActiveProtocol)
                        {
                            case SCardProtocol.T0:
                                pioSendPci = SCardPCI.T0;
                                break;
                            case SCardProtocol.T1:
                                pioSendPci = SCardPCI.T1;
                                break;
                            default:
                                throw new PCSCException(SCardError.ProtocolMismatch,
                                    "Protocol not supported: "
                                    + reader.ActiveProtocol.ToString());
                        }

                        byte[] SELECT_MF = new byte[] { 0x00, 0xA4, 0x00, 0x0C, 0x02, 0x3F, 0x00 };
                        byte[] SELECT_DF_T0 = new byte[] { 0x00, 0xA4, 0x04, 0x00, 0x07, 0xD6, 0x16, 0x00, 0x00, 0x30, 0x01, 0x01 };
                        byte[] SELECT_DF_T1 = new byte[] { 0x00, 0xA4, 0x04, 0x04, 0x07, 0xD6, 0x16, 0x00, 0x00, 0x30, 0x01, 0x01 };
                        byte[] SELECT_ELS_T0 = new byte[] { 0x00, 0xA4, 0x00, 0x00, 0x02, 0x00, 0x02 };
                        byte[] SELECT_ELS_T1 = new byte[] { 0x00, 0xA4, 0x02, 0x04, 0x02, 0x00, 0x02 };
                        byte[] READ_ELS = new byte[] { 0x00, 0xB0, 0x00, 0x00, 0xFF };

                        //--
                        byte[] pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_MF, ref pbRecvBuffer);
                        CheckErr(err);

                        Console.Write("select_mf: ");

                        //--

                        //--
                        pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_DF_T0, ref pbRecvBuffer);
                        CheckErr(err);

                        Console.Write("select_df_t0: ");
                        //--

                        //--
                        pbRecvBuffer = new byte[256];

                        //--
                        pbRecvBuffer = new byte[256];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, SELECT_ELS_T0, ref pbRecvBuffer);
                        CheckErr(err);

                        Console.Write("select_els_t0: ");
                        //--

                        //--
                        pbRecvBuffer = new byte[2048];

                        // Send SELECT command
                        err = reader.Transmit(pioSendPci, READ_ELS, ref pbRecvBuffer);
                        CheckErr(err);
                        Console.Write("read_els: ");
                        nr_indeksu = "";
                        pesel = "";
                        nazwisko = "";
                        imie = "";

                        //--


                        hContext.Release();



                        System.Collections.Generic.List<char> splitters = new System.Collections.Generic.List<char> { };

                        char[] allowedChars = new char[] { 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż' };
                        for (int i = 0; i < 18; i++)
                        {

                            splitters.Add(allowedChars[i]);
                        }
                        char[] chars = System.Text.Encoding.UTF8.GetChars(pbRecvBuffer, 0, pbRecvBuffer.Length);



                        for (int i = 0; i < chars.Length; i++)
                        {
                            if (i < 60) { chars[i] = ' '; }
                            else if (!(splitters.Contains(chars[i])) && (chars[i] < 32 || chars[i] > 122 ||(chars[i]>32 && chars[i]<47)))
                            {
                                chars[i] = ' ';
                            }
                        }
                        string str = "";

                        foreach (char a in chars)
                        {
                            Console.Write("{0:X2}", a + "\t" + Convert.ToChar(a) + "\n");
                            str += a;
                        }
                        string[] strarr2 = Regex.Split(str, "0?( ){3,}|(?<=([0-9]))( ){2,}[A-ZĄĆĘŁŃÓŚŹŻ]  |( ){2,}(?=[0-9])|(?<=[0-9][A-Z]*)( ){2,}(?=[A-Z])");
                        System.Collections.Generic.List<string> data = new System.Collections.Generic.List<string> { };
                        foreach (string str1 in strarr2)
                        {

                            if (str1 != String.Empty && str1 != " " && !(str1.Length < 3) && !str1.Contains("0s1"))
                            {
                                data.Add(str1.Replace("  ", " "));
                            }
                        }
                        string[] strarr = data.ToArray();

                        var uczelnia = strarr[1];
                        Program.nazwisko = strarr[2];
                        Program.imie = strarr[3];
                        Program.nr_indeksu = strarr[4];
                        Program.pesel = strarr[5];
                        var data_waznosci = strarr[6];
                        strarr[0] = strarr[strarr.Length - 1] = "";
                        string test = "";
                        foreach (string zxc in strarr)
                        {
                            {
                                for (int i = 0; i < 32; i++)
                                {
                                    test = zxc.Trim((Convert.ToChar(i)));
                                }
                            }
                        }
                        data_waznosci = data_waznosci.Substring(0, 4) + "-" + data_waznosci.Substring(4, 2) + "-" + data_waznosci.Substring(6, 2);
                        Console.WriteLine("Uczelnia: " + uczelnia);
                        Console.WriteLine("Data ważności: " + Convert.ToDateTime(data_waznosci));
                        Console.WriteLine("Nr indeksu: " + nr_indeksu);
                        Console.WriteLine("Pesel: " + pesel);
                        Console.WriteLine("Imie: " + imie);
                        Console.WriteLine("Nazwisko: " + nazwisko);
                        sendRequest();
                    }
                }

                catch (PCSCException ex)
                {
                    if (displayed)
                    {
                        displayed = false;
                        Console.WriteLine("Ouch: "
                            + ex.Message
                            + " (" + ex.SCardError.ToString() + ")");
                    }

                }
            }
        }

        static void sendRequest()
        {
                string IP = "localhost";
                TcpClient Client1 = new TcpClient(); // tworzymy zewnętrznego klienta, imituje on aplikację kliencką
                Client1.Connect(IP, 1500); //łączymy się z serwerem
                //
                Console.WriteLine("Nawiazano polaczenie z serwerem!");

                String imie = Program.imie;
				String nazwisko = Program.nazwisko;
				String nrindeksu = Program.nr_indeksu;
				String pesel = Program.pesel;

                proto.req myReqO = new proto.req();

                myReqO.imie = imie;
                myReqO.nazwisko = nazwisko;
                myReqO.nrindeksu = nrindeksu;
                myReqO.pesel = pesel;

                // przygotowanie danych do wysłania -> wyjście: tablica typu byte
                MemoryStream outputBuffer = new MemoryStream();
                Serializer.SerializeWithLengthPrefix<proto.req>(outputBuffer, myReqO, PrefixStyle.Fixed32, 0);
                byte[] outData = outputBuffer.ToArray();

                //

                NetworkStream ns = Client1.GetStream(); // utworzenie strumienia danych
                ns.Write(outData, 0, outData.Length);  // wysłanie danych w formie bajtów
               
				Console.WriteLine("Wyslano dane");

                Console.WriteLine("Oczekiwanie na dane...");

                byte[] odbior = new byte[1024];

                ns.Read(odbior, 0, 1024);
				
				//odbieranie danych do buforka i odczytywanie z niego
                MemoryStream inputBuffer = new MemoryStream(odbior);
                proto.res myReqI = Serializer.DeserializeWithLengthPrefix<proto.res>(inputBuffer, PrefixStyle.Fixed32);
                inputBuffer.Dispose();
                Console.WriteLine();
				//--------------------

                Console.WriteLine("Odpowiedz serwera: ");
                Console.WriteLine("Wersja: " + myReqI.version);
                Console.WriteLine("Odpowiedz: " + myReqI.odpowiedz);
                Console.WriteLine("Data: " + myReqI.data);
			        
                ns.Close();
                outputBuffer.Dispose();
        }
    }
}